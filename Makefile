NULL =

HAXE_SRC := $(wildcard src/*.hx)

TILES_STATIC = \
	beam \
	cloud_a \
	coast \
	curtain \
	lantern_room \
	mouse \
	night \
	speech_bubble \
	speech_tail \
	template \
    $(NULL)

TILES_ANIM = \
	bird_a \
	bird_b \
	candle \
	hitodama_small \
	hitodama_stand \
	waves \
	$(NULL)

KEEPER_STATIC = \
	keeper_stand \
	$(NULL)

KEEPER_ANIM = \
	keeper_lantern \
	keeper_raise_lantern \
	keeper_walk \
	$(NULL)

WIFE_STATIC = \
	wife_frown \
	wife_stand \
	wife_seagull \
	$(NULL)

WIFE_ANIM = \
	wife_metamorphose \
	$(NULL)

FONT_SOURCE = src/Amatic-Bold.ttf
FONT_PNG = res/AmaticSC.png
FONT_FNT = $(patsubst %.png,%.fnt,$(FONT_PNG))
FONT_STAMP = $(patsubst res/%.png,res/.tmp/%.stamp,$(FONT_PNG))

TILES_PNG = res/tiles.png
TILES_RES = $(patsubst %,res/.tmp/%.png,$(TILES_STATIC))
TILES_ATLAS = res/tiles.atlas

KEEPER_PNG = res/keeper.png
KEEPER_RES = $(patsubst %,res/.tmp/%.png,$(KEEPER_STATIC))

WIFE_PNG = res/wife.png
WIFE_RES = $(patsubst %,res/.tmp/%.png,$(WIFE_STATIC))

RESOURCES = \
	$(FONT_PNG) \
	$(FONT_FNT) \
	$(TILES_PNG) \
	$(KEEPER_PNG) \
	$(WIFE_PNG) \
	$(TILES_ATLAS) \
	$(NULL)


HAXE = haxe
HAXEHXML = toudaimori.hxml
HAXESERVER = --connect 6000
HAXEJSHXML = toudaimori.js.hxml
JSBIN = bin/toudaimori.js
HAXEHLHXML = toudaimori.hl.hxml
HLBIN = bin/toudaimori.hl

ZIP = zip
ZIPFLAGS = -9 -j
ZIPBIN = bin/toudaimori.zip

$(HLBIN): $(HAXEHLHXML) $(HAXEHXML) $(HAXE_SRC) | $(RESOURCES)
	$(HAXE) $(HAXESERVER) --hl $@ $< || $(HAXE) --hl $@ $<

$(JSBIN): $(HAXEJSHXML) $(HAXEHXML) $(HAXE_SRC) | $(RESOURCES)
	$(HAXE) $(HAXESERVER) --js $@ $< || $(HAXE) --js $@ $<

$(ZIPBIN): $(JSBIN) bin/index.html
	$(ZIP) $(ZIPFLAGS) $@ $^

all: $(HLBIN) $(JSBIN) dist

run: $(HLBIN)
	hl --hot-reload $(HLBIN)

dist: $(ZIPBIN)

define ANIM_template =
$2_RES += $$(shell kracountframes src/$1.kra | xargs seq --format="res/.tmp/$1_%.0f.png")
endef

define ATLAS_template =
$$(foreach _,$$($1_ANIM),$$(eval $$(call ANIM_template,$$_,$1)))

$$($1_PNG): $$($1_RES)
	packtextures $$^ $$@

endef

$(foreach _,WIFE KEEPER TILES,$(eval $(call ATLAS_template,$_)))

$(TILES_ATLAS): $(TILES_PNG) $(WIFE_PNG) $(KEEPER_PNG)
	extractatlas $^ $@

res/.tmp/%.png: src/%.kra
	kraextract $< $@

define ANIM_extract =
res/.tmp/%_$1.png: src/%.kra
	kraextract --frame $1 $$< $$@
endef

$(foreach _,0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20,$(eval $(call ANIM_extract,$_)))

$(FONT_FNT) $(FONT_PNG): $(FONT_STAMP)
$(FONT_STAMP): fonts.json $(FONT_SOURCE)
	fontgen $<
	touch $@

clean:
	rm -f $(JSBIN)
	rm -f $(HLBIN)
	rm -f $(RESOURCES)
	rm -f res/.tmp/*

watch:
	while true; do $(MAKE); inotifywait --recursive --event modify src; done

.PHONY: all clean watch dist run
