import hxd.Res;

class Night extends h2d.Bitmap {
	public function new(?parent:h2d.Object) {
		full_tile = Res.tiles.get("night");
		super(full_tile.sub(0, 0, full_tile.width, 0), parent);
		curtain = new AtlasBitmap("curtain", this);
		curtain.x = full_tile.width / 2;
		curtain.y = tile.height + 3;
		Main.ME.onReloadAtlas(reload);
	}

	public function reload() {
		full_tile = Res.tiles.get("night");
		set_tile(full_tile.sub(0, 0, tile.width, tile.height));
	}

	public function fall() {
		falling = true;
	}

	public function update(dt:Float) {
		if (fallen || !falling) {
			return;
		}
		if (tile.height >= full_tile.height) {
			fallen = true;
			falling = false;
			onFallen();
		}
		set_tile(full_tile.sub(0, 0, full_tile.width, Math.min(full_tile.height, tile.height + 105 * dt)));
		curtain.y = tile.height + 3;
	}

	public dynamic function onFallen() {}

	public var fallen = false;

	var full_tile:h2d.Tile;
	final curtain:AtlasBitmap;
	var falling = false;
}
