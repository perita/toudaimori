import hxd.Res;
import h2d.Flow;

class LanternRoom extends Stage {
	public function new(parent:h2d.Scene) {
		super(parent);

		parent.removeChildren();
		final night = new Sunrise(parent);
		final room = new AtlasBitmap("lantern_room", parent, FlowAlign.Left, FlowAlign.Top);

		final keeper = new Actor("keeper", parent);
		keeper.setPosition(125, 375);
		keeper.playAnim("lantern");

		final wife = new Actor("wife", parent);
		wife.flipX();
		wife.setPosition(parent.width + 110, 375);
		wife.playAnim("stand");

		final lantern = addInteractive(235, 210);
		lantern.setPosition(220, 0);
		lantern.onPush = (e) -> {
			say(keeper, "The Fresnel lens.");
			say(keeper, "Heart of a lighthouse.");
		};

		final descend = addInteractive(95, 120);
		descend.setPosition(545, 256);
		descend.onPush = (e) -> {
			turnRight(keeper);
			run(() -> keeper.playAnim("walk"));
			moveRight(keeper, 545, 150);
			run(() -> keeper.playAnim("lantern"));
			fadeInto(() -> new Entrance(parent))(e);
		};

		var seagull:h2d.Interactive;
		if (state.night == 3) {
			seagull = addInteractive(100, 140);
			seagull.setPosition(515, 100);
			seagull.onPush = (e) -> {
				run(disableInteractives);
				run(() -> switchTo(new Ending(parent)));
			};
		}

		beginEntry();
		switch (state.night) {
			case 1:
				if (state.crossed_over_one) {
					state.night++;

					play(Res.measured_paces);
					keeper.playAnim("stand");
					keeper.flipX();
					run(night.rise);
					run((dt) -> {
						night.update(dt);
						return !night.risen;
					});
					run(() -> play(Res.two_together));
					moveLeft(wife, 545);
					say(wife, "Good morning!");
					wait(1);
					say(keeper, "’Morning.");
					run(() -> wife.playAnim("frown"));
					say(wife, "What’s wrong?");
					run(keeper.flipX);
					wait(0.5);
					say(keeper, "There was one of them here last night.");
					say(wife, "Oh dear.");
					run(keeper.flipX);
					wait(0.5);
					say(keeper, "I could feel everything, you know?");
					say(keeper, "Their sadness.");
					say(keeper, "Their sense of loss.");
					say(keeper, "And, above all, their loneliness.");
					wait(0.5);
					say(keeper, "I felt them as if they were my own.\nAnd it’s getting worse.");
					wait(0.5);
					run(() -> wife.playAnim("stand"));
					moveLeft(wife, 245);
					wait(0.5);
					say(wife, "That’s why I’m here--to keep these feelings away.");
					wait(0.5);
					run(keeper.flipX);
					wait(0.5);
					moveRight(keeper, 190);
					wait(0.5);
					say(keeper, "I know.");
					run(fadeOut);
					say(keeper, "However....");
					run(() -> switchTo(new Transition(parent)));
				} else if (state.seen_first_intro) {
					play(Res.measured_paces);
				} else {
					state.seen_first_intro = true;
					play(Res.two_together);

					keeper.setPosition(-110, 375);
					keeper.playAnim("stand");

					moveRight(keeper, 125);
					say(keeper, "Yet another night here, taking care of things.");
					moveLeft(wife, 545);
					say(wife, "But it’s such a lovely night, don’t you think?");
					say(keeper, "Indeed, it is.");
					say(keeper, "It’s just...");
					say(keeper, "I’m getting too old for this job.");
					say(wife, "Oh, do stop grumbling.");
					say(wife, "You’re becoming an old curmudgeon.");
					say(keeper, "See?  “Old”.");
					say(wife, "Gah, it’s not what I meant.");
					say(keeper, "I know.");
					wait(1);
					say(keeper, "Say, do you think they will come tonight?");
					wait(0.5);
					say(wife, "No, they won’t.");
					say(keeper, "How do you know?");
					say(wife, "I don’t, but I have faith.");
					wait(0.5);
					say(keeper, "Let’s hope you’re right.");
					say(wife, "Of course I am!");
					say(wife, "Aren’t I always?");
					say(keeper, "Pfft.");
					say(keeper, "Yes, yes you are.");
					wait(0.5);
					say(wife, "Well, it's time for me to go to bed.");
					say(wife, "Good night.");
					say(keeper, "’Night.");
					moveRight(wife, parent.width + 150);
					run(keeper.flipX);
					run(fadeOut);
					run(() -> {
						play(Res.ominous);
						switchTo(new Ominous(this, parent));
					});
					run(fadeIn);
					run(() -> Res.door_banging.play());
					wait(1);
					run(keeper.flipX);
					say(keeper, "What was that?");
					raiseLantern(keeper);
					say(keeper, "Better check it out.");
					run(() -> play(Res.measured_paces));
					run(returnControl);
				}
			case 2:
				if (state.seen_second_intro) {
					play(Res.measured_paces);
				} else {
					state.seen_second_intro = true;
					play(Res.two_together);
					keeper.playAnim("stand");
					keeper.flipX();

					wait(1.5);
					run(() -> Res.door_banging.play());
					wait(1);
					run(() -> play(Res.measured_paces));
					run(keeper.flipX);
					say(keeper, "What? Again?");
					raiseLantern(keeper);
					say(keeper, "Sheesh! Let’s hope it’s nothing.");
					run(returnControl);
				}
			case 3:
				var hitodama:Hitodama = null;

				play(Res.two_together);
				keeper.setPosition(-110, 375);
				keeper.playAnim("stand");

				moveRight(keeper, 125);
				moveLeft(wife, 545);
				say(keeper, "Another night.");
				say(keeper, "Let’s hope this time we don’t have\nany unexpected visitor.");
				say(keeper, "I couldn’t handle three of\nthem at the same time.");
				say(wife, "No one will come tonight.");
				say(keeper, "Why are you so sure?");
				say(keeper, "Faith, again?");
				run(() -> play(Res.measured_paces));
				run(() -> wife.playAnim("frown"));
				wait(0.5);
				say(wife, "No.");
				say(wife, "Because we can’t continue like this.");
				say(wife, "You’ve become lost in this\nfantasy of yours.");
				say(keeper, "Now what are you babbling about?\nMy fantasy?");
				say(wife, "Do you remember our\nlast conversation?");
				say(wife, "Not these “imaginary conversations”,\nbut the last  REAL  conversation we had.");
				wait(1);
				run(() -> play(Res.heartbreaking));
				say(keeper, "How could I forget?");
				run(() -> switchTo(new Memory(this, parent)));
				say(keeper, "“Anyone else but you.”");
				say(keeper, "That’s what you said.");
				say(keeper, "And you meant it.");
				say(wife, "And why didn’t you say\nwhat you wanted to say?");
				say(wife, "What you  REALLY  wanted to say?");
				wait(0.5);
				say(keeper, "I don’t know.");
				say(keeper, "I don’t know why I’m the way I am.");
				say(keeper, "I felt like I  HAD  to stay.");
				say(keeper, "That what I  WANTED  was not what I  HAD  to do.");
				wait(0.5);
				say(wife, "And that inner schism\nmanifested itself these nights.");
				run(() -> {
					hitodama = new Hitodama(parent);
					hitodama.alpha = 0;
					hitodama.setPosition(Std.int(parent.width / 2), 375);
				});
				run((dt) -> {
					hitodama.alpha += dt / 1.5;
					return hitodama.alpha < 0.8;
				});
				say(wife, "In the form of desire.");
				run((dt) -> {
					return talk(hitodama.speechX, hitodama.speechY, "I wish I’ve could seen my family one last time.");
				});
				say(wife, "Regret.");
				run((dt) -> {
					return talk(hitodama.speechX, hitodama.speechY, "Is it really worth it?");
				});
				say(wife, "And, of course...");
				run((dt) -> {
					hitodama.alpha -= dt / 1.5;
					return hitodama.alpha > 0;
				});
				moveLeft(wife, Std.int(parent.width / 2));
				say(wife, "Me.");
				run((dt) -> {
					wife.alpha -= dt / 1.5;
					return wife.alpha > 0.8;
				});
				wait(0.5);
				say(wife, "You created us.");
				say(wife, "Inside your mind.");
				wait(0.5);
				say(keeper, "That’s because I miss you so much.");
				say(keeper, "I can say everything I always wanted\nwhen inside my mind.");
				say(keeper, "But when I try to externalize it....");
				say(keeper, "It’s not the same.");
				wait(0.5);
				say(keeper, "I don’t know what else to do.");
				moveRight(wife, 545);
				metamorphose(wife);
				wait(0.5);
				say(wife, "Break free from your invisible prison.");
				say(wife, "Step forward, and join me.");
				run(returnControl);
				run(enableInteractives);

				lantern.onPush = (e) -> {
					say(keeper, "The Fresnel lens.");
					say(keeper, "Does it matter now?");
				};

				descend.onPush = (e) -> {
					say(keeper, "Running away won’t work this time.");
				};
		}
		endEntry();
	}

	function metamorphose(wife:Actor) {
		run((dt) -> {
			if (wife.speed < 15) {
				wife.alpha = 1;
				wife.playAnim("metamorphose");
				wife.loop = false;
				wife.speed = 15;
			}
			if (! wife.finishedAnim()) {
				return true;
			}
			wife.speed = 5;
			wife.loop = true;
			wife.playAnim("seagull");
			wife.speechY = 275;
			wife.speechX = -30;
			return false;
		});
	}

	function raiseLantern(keeper:Actor) {
		run((dt) -> {
			if (keeper.speed < 20) {
				keeper.playAnim("raise_lantern");
				keeper.loop = false;
				keeper.speed = 20;
			}
			if (! keeper.finishedAnim()) {
				return true;
			}
			keeper.speed = 5;
			keeper.loop = true;
			keeper.playAnim("lantern");
			return false;
		});
	}
}
