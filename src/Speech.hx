import h2d.Text;

class Speech extends h2d.Interactive {
	public function new(font:h2d.Font, x:Float, y:Float, line:String, sceneWidth:Float, sceneHeight:Float, parent:h2d.Object) {
		super(sceneWidth, sceneHeight, parent);
		cursor = hxd.Cursor.Default;

		final bubble = new h2d.ScaleGrid(hxd.Res.tiles.get("speech_bubble"), 32, 32, 32, 32, this);
		final tail = new h2d.Bitmap(hxd.Res.tiles.get("speech_tail"), this);

		final text = new h2d.Text(font, bubble);
		text.setPosition(32, 0);
		text.textColor = 0xffecead5;
		text.letterSpacing = 0;
		text.smooth = true;
		text.textAlign = Align.Center;
		text.text = line;

		bubble.width = text.textWidth + 64;
		text.x = bubble.width / 2;
		bubble.height = Math.max(64, text.textHeight + 12);
		text.y = bubble.height / 2 - text.textHeight / 2 - 6;
		bubble.setPosition(Math.max(15, Math.min(x - bubble.width / 2, width - bubble.width)), Math.max(0, y - bubble.height - tail.tile.height));
		tail.setPosition(x - tail.tile.width / 2, bubble.y + bubble.height - 4);
	}

	override function onPush(e:hxd.Event) {
		visible = false;
		remove();
	}
}
