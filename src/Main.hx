import hxd.Res;

class Main extends hxd.App {
	public static var ME:Main;

	public static function main() {
		new Main();
	}

	public function new() {
		ME = this;
		super();
	}

	override function loadAssets(onLoaded:() -> Void) {
		#if js
		Res.initEmbed();
		#else
		Res.initLocal();
		#end
		onLoaded();
	}

	override function init() {
		engine.backgroundColor = 0xffecead5;
		Res.tiles.watch(reloadAtlas);
		s2d.scaleMode = LetterBox(667, 375);
		s2d.camera.clipViewport = true;

		#if debug
		state.seen_first_intro = true;
		state.seen_second_intro = true;
		state.seen_two_hitodama = true;
		state.crossed_over_one = true;
		state.night = 3;
		stage = new LanternRoom(s2d);
		#else
		stage = new Intro(s2d);
		#end
	}

	function reloadAtlas() {
		@:privateAccess Res.tiles.contents = null;
		for (callback in reloadAtlasCallbacks) {
			callback();
		}
	}

	public function onReloadAtlas(callback:() -> Void) {
		reloadAtlasCallbacks.push(callback);
	}

	override function update(dt:Float) {
		stage.update(dt);

		#if debug
		if (hxd.Key.isPressed(hxd.Key.R)) {
			final mute = isMuted();
			dispose();
			final main = new Main();
			if (mute) {
				main.toggleMute();
			}
		}
		#end
		if (hxd.Key.isPressed(hxd.Key.M)) {
			toggleMute();
		}
	}

	public function switchTo(stage:Stage) {
		this.stage = stage;
	}

	override function dispose() {
		hxd.snd.Manager.get().dispose();
		super.dispose();
	}

	function toggleMute() {
		return hxd.snd.Manager.get().masterChannelGroup.mute = !isMuted();
	}

	function isMuted() {
		return hxd.snd.Manager.get().masterChannelGroup.mute;
	}

	public function play(song:hxd.res.Sound) {
		if (song == bgm) {
			return;
		}
		if (bgm != null) {
			bgm.stop();
		}
		bgm = song;
		if (bgm != null) {
			bgm.play(true);
		}
	}

	public var stage:Stage;
	public final state = new State();

	final reloadAtlasCallbacks:Array<() -> Void> = new Array();
	var bgm:hxd.res.Sound;
}
