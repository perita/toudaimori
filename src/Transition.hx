import h2d.Flow;
import h2d.Font;
import hxd.Res;

class Transition extends Stage {
	public function new(parent:h2d.Scene, fade = true) {
		super(parent);

		parent.removeChildren();

		night = new Night(parent);
		cloud = new Cloud(parent);
		cloud.setPosition(500, 50);
		final coast = new AtlasBitmap("coast", parent, FlowAlign.Left, FlowAlign.Top);
		for (b in 0...Std.int(Math.random() * 5)) {
			newBird(50 + Math.random() * 600);
		}
		final anim = new AtlasAnim("waves", 4, parent);
		anim.setPosition(Std.int(667 / 2), 375);

		final click = addInteractive(parent.width, parent.height);
		click.onPush = (e) -> waitingClick = false;

		if (fade) {
			beginEntry();
			run(clearFade);
		}
		play(state.night > 2 ? Res.ominous : Res.clear_waters);
		run(night.fall);
		run((dt) -> {
			night.update(dt);
			return !night.fallen;
		});
		run(() -> {
			if (caption != null) {
				caption.remove();
				caption = null;
			}
			waitingClick = true;
			beam = new AtlasBitmap("beam", parent, FlowAlign.Left, FlowAlign.Top);
			caption = new Caption(Res.AmaticSC.toSdfFont(52, SDFChannel.MultiChannel, 0.5, 4 / 24), parent);
			caption.setPosition(125, 35);
			caption.text = switch (state.night) {
				case 1:
					"First  Night";
				case 2:
					"Second  Night";
				default:
					"The Last Night";
			};
		});
		run(waitCaption);
		run(enableInteractives);
		run((dt) -> waitingClick);
		run(fadeOut);
		run(() -> switchTo(new LanternRoom(parent)));
		endEntry();
	}

	function waitCaption(dt:Float):Bool {
		return caption.alpha < 1;
	}

	override function update(dt:Float) {
		cloud.update(dt);
		if (caption != null) {
			caption.update(dt);
		}
		for (bird in birds) {
			bird.update(dt);
			if (bird.x < -10) {
				birds.remove(bird);
				bird.remove();
			}
		}
		super.update(dt);
	}

	function newBird(x:Float = 680) {
		final bird = new Bird(Math.random() > 0.5 ? "a" : "b", parent);
		bird.setPosition(x, 10 + Math.random() * 80);
		birds.push(bird);
	}

	final birds:Array<Bird> = new Array();
	final night:Night;
	final cloud:Cloud;
	var beam:AtlasBitmap;
	var caption:Caption;
	var waitingClick = true;
}
