import hxd.Res;
import h2d.Flow;

class AtlasAnim extends h2d.Anim {
	public function new(animName:String, speed:Float, ?parent:h2d.Object) {
		super([], speed, parent);
		this.animName = animName;
		replay();
		Main.ME.onReloadAtlas(replay);
	}

	function replay() {
		if (animName == null) {
			return;
		}
		playAnim(animName, currentFrame);
	}

	public function playAnim(name:String, atFrame = 0.0, reverse = false) {
		animName = name;
		final frames = anim(animName);
		if (reverse) {
			frames.reverse();
		}
		play(frames, Math.min(frames == null ? 0 : frames.length, currentFrame));
		if (xFlip) {
			flipFrames();
		}
	}

	function anim(name:String) {
		return Res.tiles.getAnim(name, FlowAlign.Middle, FlowAlign.Bottom);
	}

	public function flipX() {
		xFlip = !xFlip;
		flipFrames();
	}

	function flipFrames() {
		for (frame in frames) {
			if (frame == null) {
				continue;
			}
			frame.flipX();
		}
	}

	public var xFlip = false;

	var animName:String;
}
