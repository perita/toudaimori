class Actor extends AtlasAnim {
	public var speechX(get, set):Float;
	public var speechY(get, set):Float;

	public function new(name:String, ?parent:h2d.Object) {
		super(null, 5, parent);
		prefix = name + '_';
		xSpeech = switch (name) {
			case "wife": -15;
			default: 0;
		};
		ySpeech = switch (name) {
			case "wife": 250;
			case "hitodama": 200;
			default: 300;
		}
	}

	override function anim(name:String) {
		return super.anim(prefix + name);
	}

	public function finishedAnim(): Bool {
		return currentFrame == frames.length;
	}

	function get_speechX():Float {
		return x - xSpeech;
	}

	function set_speechX(x:Float):Float {
		xSpeech = x;
		return xSpeech;
	}

	function get_speechY():Float {
		return y - ySpeech;
	}

	function set_speechY(y:Float):Float {
		ySpeech = y;
		return ySpeech;
	}

	final prefix:String;
	var ySpeech:Float;
	var xSpeech:Float;
}
