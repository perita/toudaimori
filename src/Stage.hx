import h2d.Scene;
import h2d.Font;
import hxd.Res;
import h2d.Interactive;

abstract class Stage {
	function new(parent:Scene) {
		this.parent = parent;
		font = Res.AmaticSC.toSdfFont(42, SDFChannel.MultiChannel, 0.5, 4 / 24);
	}

	public function update(dt:Float):Void {
		while (!actions.isEmpty()) {
			final action = actions.first();
			if (action(dt)) {
				break;
			}
			actions.pop();
			if (Main.ME.stage != this) {
				break;
			}
		}
	}

	overload extern inline function run(action:(Float) -> Bool) {
		actions.add(action);
	}

	overload extern inline function run(action:() -> Void) {
		actions.add((dt) -> {
			action();
			return false;
		});
	}

	function say(actor:Actor, line:String, ?parent:h2d.Object) {
		run((dt) -> {
			return talk(actor.speechX, actor.speechY, line, parent);
		});
	}

	function talk(x:Float, y:Float, line:String, ?parent:h2d.Object):Bool {
		if (speech == null) {
			speech = new Speech(font, x, y, line, this.parent.width, this.parent.height, parent == null ? this.parent : parent);
		}
		if (speech.visible) {
			return true;
		}
		speech = null;
		return false;
	}

	function fadeIn(dt:Float):Bool {
		if (fade == null) {
			fade = new Fade(parent);
			fade.alpha = 1;
		}
		fade.alpha -= dt / 1.5;
		if (fade.alpha <= 0) {
			return false;
		}
		return true;
	}

	function fadeOut(dt:Float):Bool {
		if (fade == null) {
			fade = new Fade(parent);
		}
		fade.alpha += dt / 1.5;
		return fade.alpha < 1;
	}

	function clearFade() {
		if (fade == null) {
			return;
		}
		fade.remove();
		fade = null;
	}

	function fadeInto(newStage:() -> Stage):(hxd.Event) -> Void {
		return (e) -> {
			run(fadeOut);
			run(() -> switchTo(newStage()));
		};
	}

	function beginEntry() {
		fade = new Fade(parent);
		fade.alpha = 1;

		run(disableInteractives);
		run(fadeIn);
	}

	function endEntry() {
		run(clearFade);
		run(enableInteractives);
	}

	function moveRight(obj:h2d.Object, target:Float, speed:Float = 300) {
		run((dt) -> {
			obj.x += dt * speed;
			return obj.x < target;
		});
	}

	function moveLeft(obj:h2d.Object, target:Float, speed:Float = 300) {
		run((dt) -> {
			obj.x -= dt * speed;
			return obj.x > target;
		});
	}

	function turnRight(actor:Actor) {
		run(() -> {
			if (actor.xFlip) {
				actor.flipX();
			}
		});
	}

	function turnLeft(actor:Actor) {
		run(() -> {
			if (!actor.xFlip) {
				actor.flipX();
			}
		});
	}

	function wait(seconds:Float) {
		run((dt:Float) -> {
			seconds -= dt;
			return seconds > 0;
		});
	}

	function addInteractive(width:Float, height:Float, ?parent:h2d.Object):Interactive {
		final interactive = new Interactive(width, height, parent == null ? this.parent : parent);
		interactives.push(interactive);
		#if debug
		final rect = new h2d.Bitmap(h2d.Tile.fromColor(0xffff0000), interactive);
		rect.alpha = 0.5;
		rect.width = interactive.width;
		rect.height = interactive.height;
		#end
		return interactive;
	}

	function enableInteractives(dt:Float):Bool {
		for (interactive in interactives) {
			interactive.cursor = hxd.Cursor.Button;
			interactive.visible = true;
		}
		return false;
	}

	function disableInteractives(dt:Float):Bool {
		for (interactive in interactives) {
			interactive.cursor = hxd.Cursor.Default;
			interactive.visible = false;
		}
		return false;
	}

	function returnControl(dt:Float):Bool {
		if (control == null) {
			Res.click.play(false);
			control = createControl();
			control.setPosition(parent.width / 2 - control.width / 2, parent.height / 2 - control.height / 2);
		}
		if (control.update(dt)) {
			return true;
		}
		control.remove();
		control = null;
		return false;
	}

	function createControl():Control {
		return new Control(font, parent);
	}

	function switchTo(stage:Stage) {
		Main.ME.switchTo(stage);
	}

	function play(song:hxd.res.Sound) {
		Main.ME.play(song);
	}

	var state(get, null):State;

	function get_state():State {
		return Main.ME.state;
	}

	var control:Control = null;
	var speech:Speech = null;
	final actions:List<(Float) -> Bool> = new List();
	final font:Font;
	final parent:Scene;
	var fade:Fade;
	final interactives:Array<Interactive> = [];
}
