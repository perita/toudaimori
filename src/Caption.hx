import h2d.Font;

class Caption extends h2d.Text {
	public function new(font:Font, ?parent:h2d.Object) {
		super(font, parent);
		letterSpacing = 0;
		textColor = 0xff000000;
		smooth = true;
		alpha = 0;
		wait = 0.5;
		dir = 1;
	}

	public function update(dt:Float) {
		if (wait > 0) {
			wait -= dt;
		} else if (dir > 0 && alpha < 1) {
			alpha += dt / 1.5;
		} else if (dir < 0 && alpha >= 0) {
			alpha -= dt / 1.5;
		}
	}

	public function fadeOff() {
		dir = -1;
	}

	var wait:Float;
	var dir:Float;
}
