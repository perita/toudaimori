import hxd.Res;

class Cloud extends AtlasBitmap {
	public function new(?parent:h2d.Object) {
		super("cloud_a", parent);
	}

	public function update(dt:Float) {
		x -= dt;
	}
}
