class Bird extends AtlasAnim {
	public function new(kind:String, ?parent:h2d.Object) {
		super("bird_" + kind, 4, parent);
		v = Math.random() * Math.PI / 2;
	}

	public function update(dt:Float) {
		x -= dt * 20;
		v += dt;
		y += Math.sin(v) / 4;
	}

	public var v:Float;
}
