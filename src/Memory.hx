import h2d.Flow;
import h2d.Text;
import h2d.Object;
import hxd.Res;

class Memory extends Stage {
	public function new(prevStage:Stage, parent:h2d.Scene) {
		super(parent);
		this.prevStage = prevStage;

		root = new Object(parent);
		root.y = -parent.height - 50;

		final room = new AtlasBitmap("template", root, FlowAlign.Left, FlowAlign.Top);

		final keeper = new Actor("keeper", root);
		keeper.flipX();
		keeper.playAnim("stand");
		keeper.setPosition(Std.int(parent.width / 2), 375);

		final wife = new Actor("wife", root);
		wife.playAnim("frown");
		wife.setPosition(125, 375);

		run((dt) -> {
			parent.y = Math.min(-root.y, parent.y + dt * 100);
			return parent.y < -root.y;
		});
		wait(0.5);
		say(wife, "You might be content with this\nmonotonous life, but I need something else.", root);
		say(keeper, "It’s important work that\nsomebody has to do.", root);
		say(wife, "Important for whom?", root);
		say(wife, "When’s the last time you’ve seen\na ship around here?", root);
		wait(1);
		say(keeper, "This is what I’m good at.", root);
		say(keeper, "What else could I do?", root);
		say(wife, "You can do anything you want to,\nthe world’s your oyster.", root);
		wait(1);
		say(wife, "So, what’s your answer?", root);
		createChoices([
			"Let’s go you and me, both.",
			"Without you there is no point in staying.",
			"No matter where we are, as long as we are together."
		]);
		run(returnControl);
		run(enableInteractives);
		run(waitChoice);
		run((dt) -> talk(keeper.speechX, keeper.speechY, choice, root));
		wait(1);
		say(keeper, "Sorry, but I must stay; I’m the keeper.", root);
		wait(1);
		say(wife, "It’s decided, then.", root);
		moveRight(wife, 545);
		wait(1);
		run(wife.flipX);
		moveLeft(wife, 545 - 25);
		say(wife, "Do you know what the worst is?", root);
		say(wife, "That I love you.", root);
		say(wife, "But I don’t want to.", root);
		say(wife, "Not you; anyone else but you.", root);
		run(wife.flipX);
		say(wife, "Goodbye.", root);
		moveRight(wife, parent.width + 110);
		wait(2);
		run((dt) -> {
			parent.y = Math.max(0, parent.y - dt * 100);
			return parent.y > 0;
		});
		run(() -> {
			root.remove();
			switchTo(prevStage);
		});
	}

	override function createControl():Control {
		return new Control(font, root);
	}

	function createChoices(lines:Array<String>) {
		run(() -> {
			var y = 32.0;
			final middle = Std.int(parent.width / 2);

			for (line in lines) {
				final bubble = new h2d.ScaleGrid(Res.tiles.get("speech_bubble"), 32, 32, 32, 32, root);
				bubble.alpha = 0.8;

				final text = new h2d.Text(font, bubble);
				text.setPosition(32, 0);
				text.textColor = 0xffecead5;
				text.letterSpacing = 0;
				text.smooth = true;
				text.textAlign = Align.Center;
				text.text = line;

				bubble.width = text.textWidth + 64;
				text.x = Std.int(bubble.width / 2);
				bubble.height = Math.max(64, text.textHeight + 12);
				text.y = Std.int(bubble.height / 2 - text.textHeight / 2 - 6);
				bubble.setPosition(Std.int(middle - bubble.width / 2), y);

				final click = addInteractive(bubble.width, bubble.height, bubble);
				click.onOver = (e) -> bubble.alpha = 1;
				click.onOut = (e) -> bubble.alpha = 0.8;
				click.onClick = (e) -> choice = line.split(" ")[0] + "...";

				choices.push(bubble);
				y += bubble.height + 32;
			}

			disableInteractives(0);
		});
	}

	function waitChoice(dt:Float):Bool {
		if (choice == "") {
			return true;
		}
		for (choice in choices) {
			choice.remove();
		}
		choices.splice(0, choices.length);
		return false;
	}

	final root:Object;
	final prevStage:Stage;
	final choices = new Array<h2d.Object>();
	var choice = "";
}
