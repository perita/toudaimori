import h2d.Text.Align;
import h2d.Flow;
import hxd.Res;
import h2d.Font;
import h2d.Interactive;

class Ending extends Stage {
	public function new(parent:h2d.Scene) {
		super(parent);

		play(Res.clear_waters);

		run(fadeOut);
		run(firstCaption);
		run(fadeInCaption);
		run(fadeOutCaption);
		run(setupLastScene);
		run(waitBirds);
		run(secondCaption);
		run(fadeInCaption);
		run(fadeOutCaption);
		run(lastCaption);
		run(fadeInCaption);
	}

	function firstCaption(dt:Float):Bool {
		return setupCaption(font, "I took that step forward;\nI left my old life behind.");
	}

	function setupCaption(font:Font, text:String) {
		parent.removeChildren();
		caption = new Caption(font, parent);
		caption.textAlign = Align.Center;
		caption.text = text;
		caption.setPosition(parent.width / 2, parent.height / 2 - caption.textHeight);
		return false;
	}

	function secondCaption(dt:Float):Bool {
		return setupCaption(font, "And I am no longer alone.");
	}

	function lastCaption(dt:Float):Bool {
		return setupCaption(Res.AmaticSC.toSdfFont(63, SDFChannel.MultiChannel, 0.5, 4 / 24), "The End");
	}

	function setupLastScene(dt:Float):Bool {
		parent.removeChildren();
		birdA = new Bird("a", parent);
		birdA.setPosition(460, 50);
		birdA.v = Math.PI;
		birdB = new Bird("b", parent);
		birdB.setPosition(460, 60);
		birdB.v = birdA.v;
		final coast = new AtlasBitmap("coast", parent, FlowAlign.Left, FlowAlign.Top);
		final anim = new AtlasAnim("waves", 4, parent);
		anim.setPosition(Std.int(667 / 2), 375);
		fade = new Fade(parent);
		fade.alpha = 1;
		Res.pigeon_wings.play();
		return false;
	}

	function waitBirds(dt:Float):Bool {
		final fadeStart = 120;
		if (birdA.x > fadeStart || birdB.x > fadeStart) {
			if (fade.alpha > 0) {
				fade.alpha -= dt / 1.5;
			}
		} else {
			fade.alpha += dt / 1.5;
		}
		birdA.update(dt);
		birdB.update(dt);
		return birdA.x > fadeStart || birdB.x > fadeStart || fade.alpha < 1;
	}

	function fadeInCaption(dt:Float):Bool {
		caption.update(dt);
		if (caption.alpha < 1) {
			return true;
		}
		click = new Interactive(parent.width, parent.height, parent);
		click.onClick = (e) -> {
			click.remove();
			caption.fadeOff();
		}
		return false;
	}

	function fadeOutCaption(dt:Float):Bool {
		caption.update(dt);
		if (caption.alpha > 0) {
			return true;
		}
		caption.remove();
		return false;
	}

	var birdA:Bird;
	var birdB:Bird;
	var caption:Caption;
	var click:Interactive;
}
