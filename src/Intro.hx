import h2d.Flow;
import h2d.Font;
import h2d.Interactive;
import hxd.Res;

class Intro extends Transition {
	public function new(parent:h2d.Scene) {
		super(parent, false);

		caption = new Caption(Res.AmaticSC.toSdfFont(63, SDFChannel.MultiChannel, 0.5, 4 / 24), parent);
		caption.setPosition(45, 45);
		caption.text = "The  Lighthouse  Keeper";

		final oldQueue = new List<(Float) -> Bool>();
		while (!actions.isEmpty()) {
			oldQueue.add(actions.pop());
		}

		run(disableInteractives);
		run(waitCaption);
		run(() -> waitingClick = true);
		run(enableInteractives);
		run((dt) -> waitingClick);
		run(disableInteractives);

		while (!oldQueue.isEmpty()) {
			actions.add(oldQueue.pop());
		}
	}

	override function update(dt:Float) {
		super.update(dt);
		if (fade == null) {
			if (Math.random() < 0.0025) {
				newBird();
			}
		}
	}
}
