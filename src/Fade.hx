class Fade extends h2d.Bitmap {
	public function new(parent:h2d.Scene) {
		super(h2d.Tile.fromColor(0xffecead5), parent);
		alpha = 0;
		width = parent.width;
		height = parent.height;
	}
}
