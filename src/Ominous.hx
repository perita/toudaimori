import h2d.Flow;

class Ominous extends Stage {
	public function new(prevStage:Stage, parent:h2d.Scene) {
		super(parent);
		this.prevStage = prevStage;

		night = new AtlasBitmap("night", parent, FlowAlign.Left, FlowAlign.Top);
		coast = new AtlasBitmap("coast", parent, FlowAlign.Left, FlowAlign.Top);
		waves = new AtlasAnim("waves", 4, parent);
		waves.setPosition(Std.int(667 / 2), 375);
		beam = new AtlasBitmap("beam", parent, FlowAlign.Left, FlowAlign.Top);
		hitodama = new Actor("hitodama", parent);
		hitodama.playAnim("small");
		hitodama.setPosition(-40, 375);
		
		beginEntry();
		run(waitHitodama);
		run(fadeOut);
		run(returnToPrev);
	}

	function waitHitodama(dt:Float):Bool {
		hitodama.x += 50 * dt;
		hitodama.y -= 15 * dt;
		return hitodama.x < 450;
	}

	function returnToPrev(dt:Float):Bool {
		clearFade();
		hitodama.remove();
		beam.remove();
		waves.remove();
		coast.remove();
		night.remove();
		switchTo(prevStage);
		return false;
	}

	final prevStage:Stage;
	final night:AtlasBitmap;
	final coast:AtlasBitmap;
	final beam:AtlasBitmap;
	final waves:AtlasAnim;
	final hitodama:Actor;
}
