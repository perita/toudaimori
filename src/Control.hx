import h2d.Text;
import h2d.Flow;

class Control extends h2d.ScaleGrid {
	public function new(font:h2d.Font, ?parent:h2d.Object) {
		super(hxd.Res.tiles.get("speech_bubble"), 32, 32, 32, 32, parent);

		final text = new Text(font, this);
		text.setPosition(32, 0);
		text.textColor = 0xffecead5;
		text.letterSpacing = 0;
		text.smooth = true;
		text.textAlign = Align.Left;
		text.text = "Control";

		final mouse = new AtlasBitmap("mouse", this, FlowAlign.Left, FlowAlign.Top);

		text.x = 32;
		mouse.x = text.x + text.textWidth + 8;
		this.width = mouse.x + mouse.tile.width + 32;
		this.height = Math.max(64, text.textHeight + 12);
		text.y = this.height / 2 - text.textHeight / 2 - 6;
		mouse.y = this.height / 2 - mouse.tile.height / 2;
	}

	public function update(dt:Float):Bool {
		toggle -= dt;
		if (toggle <= 0) {
			if (loops == 0) {
				return false;
			}
			loops--;
			visible = !visible;
			toggle += 0.5;
		}
		return true;
	}

	var toggle = 0.5;
	var loops = 4;
}
