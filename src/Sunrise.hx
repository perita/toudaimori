import hxd.Res;

class Sunrise extends h2d.Bitmap {
	public function new(?parent:h2d.Object) {
		full_tile = Res.tiles.get("night");
		super(full_tile.sub(0, 0, full_tile.width, full_tile.height), parent);
		curtain = new AtlasBitmap("curtain", this);
		curtain.x = full_tile.width / 2;
		curtain.y = tile.height + curtain.tile.height + 3;
		Main.ME.onReloadAtlas(reload);
	}

	public function reload() {
		full_tile = Res.tiles.get("night");
		set_tile(full_tile.sub(0, 0, tile.width, tile.height));
	}

	public function rise() {
		rising = true;
	}

	public function update(dt:Float) {
		if (tile.height <= 0) {
			risen = true;
			rising = false;
		}
		if (risen || !rising) {
			return;
		}
		set_tile(full_tile.sub(0, 0, full_tile.width, Math.max(0, tile.height - 105 * dt)));
		curtain.y = tile.height + 3;
	}

	public var risen = false;

	var full_tile:h2d.Tile;
	final curtain:AtlasBitmap;
	var rising = false;
}
