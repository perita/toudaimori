class Hitodama extends Actor {
	public function new(?parent:h2d.Object) {
		super("hitodama", parent);
		playAnim("stand");
		change = Math.random();
	}

	override function sync(ctx:h2d.RenderContext) {
		super.sync(ctx);
		y += ctx.elapsedTime * 10 * dir;
		change -= ctx.elapsedTime;
		if (change <= 0) {
			change += 1;
			dir = -dir;
		}
	}

	var dir = 1;
	var change:Float;
}
