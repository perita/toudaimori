import h2d.Flow;
import hxd.Res;

class Entrance extends Stage {
	public function new(parent:h2d.Scene) {
		super(parent);

		parent.removeChildren();
		night = new Sunrise(parent);
		final room = new AtlasBitmap("template", parent, FlowAlign.Left, FlowAlign.Top);

		final candle = new AtlasAnim("candle", 4, parent);
		candle.setPosition(461, 41);

		keeper = new Actor("keeper", parent);
		keeper.playAnim("lantern");

		upstairs = addInteractive(350, 100);
		upstairs.onPush = fadeInto(() -> new LanternRoom(parent));

		final window = addInteractive(75, 100);
		window.setPosition(535, 0);
		window.onPush = (e) -> say(keeper, "Outside, the night is deep and wide.");

		switch (state.night) {
			case 1:
				keeper.setPosition(125, 375);

				final hitodama = new Hitodama(parent);
				if (state.seen_one_hitodama) {
					hitodama.setPosition(570, 375);
				} else {
					hitodama.setPosition(parent.width + 20, 375);
				}

				final hitodamaTouch = addHitodamaTouch(hitodama);
				hitodamaTouch.onPush = (e) -> interactWithHitodama(hitodama);

				beginEntry();
			case 2:
				keeper.setPosition(Std.int(667 / 2), 375);

				final firstHitodama = new Hitodama(parent);
				firstHitodama.setPosition(570, 375);

				final secondHitodama = new Hitodama(parent);
				secondHitodama.flipX();
				secondHitodama.setPosition(100, 375);

				final firstHitodamaTouch = addHitodamaTouch(firstHitodama);
				firstHitodamaTouch.onPush = (e) -> interactWithTwoHitodama(firstHitodama, secondHitodama, false);

				final secondHitodamaTouch = addHitodamaTouch(secondHitodama);
				secondHitodamaTouch.onPush = (e) -> interactWithTwoHitodama(secondHitodama, firstHitodama, true);

				beginEntry();
				if (!state.seen_two_hitodama) {
					state.seen_two_hitodama = true;

					say(keeper, "What in the world?");
					lookAround();
					say(keeper, "There are two of them, now?");
					run(keeper.flipX);
					run(returnControl);
				}
		}

		play(Res.measured_paces);
		endEntry();
	}

	function addHitodamaTouch(hitodama:Actor):h2d.Interactive {
		final hitodamaTouch = addInteractive(150, 200, hitodama);
		hitodamaTouch.setPosition(-80, -225);
		return hitodamaTouch;
	}

	function interactWithHitodama(hitodama:Actor) {
		if (state.seen_one_hitodama) {
			state.crossed_over_one = true;
			run(disableInteractives);
			say(keeper, "Who are you?");
			say(hitodama, "I...");
			wait(0.5);
			say(hitodama, "I don’t remember.");
			say(keeper, "What do you remember?");
			wait(1);
			say(hitodama, "Cold.");
			say(hitodama, "I remember being cold.");
			say(keeper, "What else?");
			wait(1);
			say(hitodama, "I was going...");
			wait(0.5);
			say(hitodama, "Home.");
			say(hitodama, "I was returning back home.");
			wait(0.5);
			say(hitodama, "To my family.");
			say(hitodama, "Then, everything went cold.");
			say(hitodama, "And dark.");
			wait(0.5);
			say(hitodama, "But then I saw a light.");
			say(hitodama, "Next thing I knew, I was standing here.");
			wait(1);
			say(keeper, "I’m sorry.");
			say(keeper, "I really am.");
			moveLeft(hitodama, Std.int(667 / 2));
			wait(0.5);
			say(hitodama, "I wish I’ve could seen my family one last time.");
			say(hitodama, "Would you say goodbye\nto them, for me?");
			wait(1);
			say(keeper, "I will.");
			say(hitodama, "Thank you.");
			crossOver(hitodama);
			wait(0.5);
			say(keeper, "That was...");
			say(keeper, "Not good.");
			run(returnControl);
			run(enableInteractives);
		} else {
			state.seen_one_hitodama = true;
			run(disableInteractives);
			say(keeper, "Who goes there?");
			moveLeft(hitodama, 570);
			say(keeper, "Good grief!");
			wait(0.5);
			say(keeper, "Well, you were wrong this time, Lily.");
			run(enableInteractives);
		}
	}

	function interactWithTwoHitodama(firstHitodama:Actor, secondHitodama:Actor, flip:Bool) {
		var secondSpeech:Speech;

		final toLeft = flip ?(hd, offset) -> moveRight(hd, hd.x + offset) : (hd, offset) -> moveLeft(hd, hd.x - offset);
		final toRight = flip ?(hd, offset) -> moveLeft(hd, hd.x - offset) : (hd, offset) -> moveRight(hd, hd.x + offset);

		run(disableInteractives);
		if (flip) {
			run(keeper.flipX);
		}
		say(keeper, "How may I help you?");
		say(firstHitodama, "We haven’t come for help.");
		run(keeper.flipX);
		say(secondHitodama, "We’ve come  TO  help.");
		say(keeper, "To help? To help whom?");
		run(() -> secondSpeech = new Speech(font, firstHitodama.speechX, firstHitodama.speechY, "You!", parent.width, parent.height, parent));
		say(secondHitodama, "You!");
		run(() -> {
			secondSpeech.remove();
			secondSpeech = null;
		});
		say(keeper, "Me?");
		lookAround(0.25);
		wait(0.5);
		say(keeper, "I don’t need any help!");
		toLeft(firstHitodama, 25);
		say(firstHitodama, "Are you sure?");
		toRight(secondHitodama, 25);
		run(keeper.flipX);
		say(secondHitodama, "Then why did you create us?");
		say(keeper, "What nonsense!");
		say(keeper, "I didn’t  “create”  you.");
		toLeft(firstHitodama, 50);
		run(keeper.flipX);
		say(firstHitodama, "Then why are we here?");
		say(keeper, "Because you’re attracted to the lighthouse’s light.");
		wait(0.5);
		say(keeper, "...aren’t you?");
		toRight(secondHitodama, 50);
		run(keeper.flipX);
		say(secondHitodama, "Then why keep the light burning\nnight after night?");
		say(keeper, "Somebody has to do it!");
		say(keeper, "I’m the lighthouse keeper, for heaven’s sake.");
		say(keeper, "It’s my duty!");
		toLeft(firstHitodama, 75);
		run(keeper.flipX);
		say(firstHitodama, "Is it really worth it?");
		toRight(secondHitodama, 75);
		run(keeper.flipX);
		say(secondHitodama, "The miserable days?");
		toLeft(firstHitodama, 100);
		run(keeper.flipX);
		say(firstHitodama, "The lonely nights?");
		toRight(secondHitodama, 100);
		run(keeper.flipX);
		say(secondHitodama, "Living a wasted life?");
		say(keeper, "Yes!");
		say(keeper, "This is what I  HAVE  to do.");
		say(keeper, "I  AM  the lighthouse keeper.");
		say(keeper, "This is  MY  life; without this\nI’m  NOTHING!");
		wait(1);
		run(keeper.flipX);
		say(firstHitodama, "If that’s what you want to believe.");
		crossOver(firstHitodama);
		run(keeper.flipX);
		say(secondHitodama, "Maybe we can’t help you, after all.");
		crossOver(secondHitodama);
		if (!flip) {
			run(keeper.flipX);
		}
		say(keeper, "What in the blazes was all that about?");
		upstairs.onPush = endSecondNight;
		run(returnControl);
		run(enableInteractives);
	}

	function crossOver(hitodama:Actor) {
		run(() -> Res.cross_over.play());
		run((dt) -> {
			hitodama.alpha -= dt / 1.5;
			if (hitodama.alpha > 0) {
				return true;
			}
			hitodama.remove();
			return false;
		});
	}

	function lookAround(speed = 0.5) {
		run(keeper.flipX);
		wait(speed);
		run(keeper.flipX);
		wait(speed);
		run(keeper.flipX);
	}

	override function update(dt:Float) {
		super.update(dt);
		night.update(dt);
	}

	function endSecondNight(e) {
		state.night++;

		final wife = new Actor("wife", parent);
		wife.flipX();
		wife.setPosition(parent.width + 110, 375);
		wife.playAnim("frown");

		run(disableInteractives);
		run(keeper.flipX);
		wait(0.25);
		run((dt) -> {
			return talk(parent.width - 50, wife.speechY, "Adam, wait.");
		});
		run(keeper.flipX);
		say(keeper, "Lily?");
		run(() -> play(Res.two_together));
		moveLeft(keeper, 125);
		moveLeft(wife, 545);
		say(wife, "This is getting out of hand, don’t you think?");
		say(keeper, "Are you saying this is my fault?");
		say(keeper, "I didn’t invite those two!");
		wait(0.25);
		say(wife, "In a way, you did.");
		wait(0.25);
		say(keeper, "How? Tell me how I did it.");
		run(night.rise);
		wait(0.5);
		say(wife, "Maybe this isn’t the moment.");
		say(wife, "We’ll talk tomorrow");
		moveRight(wife, parent.width + 110);
		wait(0.25);
		lowerLantern(keeper);
		wait(0.25);
		say(keeper, "Go figure.");
		run(fadeOut);
		run(() -> switchTo(new Transition(parent)));
	}

	function lowerLantern(keeper:Actor) {
		run((dt) -> {
			if (keeper.speed < 20) {
				keeper.playAnim("raise_lantern", 0, true);
				keeper.loop = false;
				keeper.speed = 20;
			}
			if (! keeper.finishedAnim()) {
				return true;
			}
			keeper.speed = 5;
			keeper.loop = true;
			keeper.playAnim("stand");
			return false;
		});
	}

	final keeper:Actor;
	final upstairs:h2d.Interactive;
	final night:Sunrise;
}
